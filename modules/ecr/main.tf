#ECR repository
resource "aws_ecr_repository" "this" {
    count = var.create_ecr_repos ? 1 : 0
    name = var.ecr_repository_name
    image_tag_mutability = var.image_tag_mutability
    tags = var.tags
    dynamic "encryption_configuration" {
        for_each = try(var.encryption_configuration, "")
        content {
            encryption_type = try(encryption_configuration.value.encryption_type, "AES256")
            kms_key = try(encryption_configuration.value.kms_key, "")
        }
    }
    dynamic "image_scanning_configuration" {
        for_each = try(var.image_scanning_configuration, "")
        content {
            scan_on_push = try(image_scanning_configuration.value.scan_on_push, false)
        }
    }
} 