output "vpc_endpoint_id" {
    description = "The ID of the VPC endpoint."
    value = try(aws_vpc_endpoint.this.*.id, "")
}

output "vpc_endpoint_arn" {
    description = "The Amazon Resource Name (ARN) of the VPC endpoint."
    value = try(aws_vpc_endpoint.this.*.arn, "")
}

output "vpc_endpoint_cidr_blocks" {
    description = "The list of CIDR blocks for the exposed AWS service."
    value = try(aws_vpc_endpoint.this.*.cidr_blocks, "")
}