variable "create_vpc_endpoint" {
    type = bool
    description = "Whether to create vpc endpoint"
    default = false
}

variable "service_name" {
    type = string
    description = "The service name."
    default = ""
}

variable "vpc_id" {
    type = string
    description = "The ID of the VPC in which the endpoint will be used."
    default = ""
}

variable "auto_accept" {
    type = bool
    description = "Accept the VPC endpoint"
    default = false
}

variable "policy" {
    type = any
    description = "A policy to attach to the endpoint that controls access to the service."
    default = null
}

variable "private_dns_enabled" {
    type = bool
    description = "Whether or not to associate a private hosted zone with the specified VPC."
    default = false
}

variable "ip_address_type" {
    type = string
    description = "The IP address type for the endpoint."
    default = "ipv4"
}

variable "route_table_ids" {
    type = list(string)
    description = "One or more route table IDs."
    default = []
}

variable "subnet_ids" {
    type = list(string)
    description = "The ID of one or more subnets in which to create a network interface for the endpoint."
    default = []
}

variable "security_group_ids" {
    type = list(string)
    description = "The ID of one or more security groups to associate with the network interface."
    default = []
}

variable "tags" {
    type = map(string)
    description = "Tags"
    default = {}
}

variable "vpc_endpoint_type" {
    type = string
    description = "The VPC endpoint type"
    default = ""
}

variable "dns_options" {
    type = any
    description = "The DNS options for the endpoint."
    default = {}
}