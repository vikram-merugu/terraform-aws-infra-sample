variable "create_codecommit_repository" {
    type = bool
    description = "Whether to create codecommit repository"
    default = false
}

variable "codecommit_repository_name" {
    type = string
    description = "The name for the repository."
    default = ""
}

variable "codecommit_repos_description" {
    type = string
    description = "Description for codecommit repository"
    default = ""
}

variable "repos_default_branch" {
    type = string
    description = "The default branch of the repository."
    default = "main"
}

variable tags {
    type = map(string)
    description = "Key-value map of resource tags."
    default = {
        Name = "self-test"
    }
}