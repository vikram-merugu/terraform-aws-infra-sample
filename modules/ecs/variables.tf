variable "create_ecs_cluster" {
    type = bool
    description = "Whether to create ECS cluster"
    default = false
}

variable "cluster_name" {
    type = string
    description = "Name of the cluster"
    default = ""
}

variable "configuration"{
    type = map(any)
    description = "cluster configuration"
    default = {}
}
#capacity provider for cluster
variable "capacity_providers" {
    type = list(string)
    description = "Set of names of one or more capacity providers to associate with the cluster"
    default = []
}

variable "default_capacity_provider_strategy" {
    type = any
    description = "Default capacity provider strategy"
    default = []
}

#Service
variable "create_ecs_service" {
    type = bool
    description = "Whether to create ECS service"
    default = false
}

variable "service_name" {
    type = string
    description = "Name of ECS service"
    default = null
}

variable "alarms" {
    type = map(any)
    description = "CloudWatch alarm"
    default = {}
}

variable "capacity_provider_strategy" {
    type = any
    description = "Capacity provider strategies to use for the service."
    default = []
}

variable "deployment_circuit_breaker" {
    type = map(any)
    description = "Configuration block for deployment circuit breaker."
    default = {}
}

variable "deployment_controller" {
    type = map(any)
    description = "Configuration block for deployment controller configuration."
    default = {}
}

variable "deployment_maximum_percent" {
    type = number
    description = "Upper limit of the number of running tasks that can be running in a service during a deployment."
    default = null
}

variable "deployment_minimum_healthy_percent" {
    type = number
    description = "Lower limit of the number of running tasks that must remain running and healthy in a service during a deployment."
    default = null
}

variable "desired_count" {
    type = number
    description = "Number of instances of the task definition to place and keep running."
    default = 0
}

variable "enable_ecs_managed_tags" {
    type = bool
    description = "Specifies whether to enable Amazon ECS managed tags for the tasks within the service."
    default = false
}

variable "enable_execute_command" {
    type = bool
    description = "Specifies whether to enable Amazon ECS Exec for the tasks within the service."
    default = false 
}

variable "force_new_deployment" {
    type = bool
    description = "Enable to force a new task deployment of the service"
    default = false
}

variable "health_check_grace_period_seconds" {
    type = number
    description = "econds to ignore failing load balancer health checks on newly instantiated tasks to prevent premature shutdown, up to 2147483647"
    default = null
}

variable "iam_role" {
    type = string
    description = "ARN of the IAM role that allows Amazon ECS to make calls to your load balancer on your behalf"
    default = ""
}

variable "launch_type" {
    type = string
    description = "Launch type on which to run your service"
    default = "EC2"
}

variable "load_balancer" {
    type = any
    description = "Configuration block for load balancers"
    default = null
}

variable "network_configuration" {
    type = any
    description = "Network configuration for the service"
    default = null
}

variable "ordered_placement_strategy" {
    type = map(any)
    description = "Service level strategy rules that are taken into consideration during task placement"
    default = {}
}

variable "placement_constraints" {
    type = map(any)
    description = "Rules that are taken into consideration during task placement"
    default = {}
}

variable "platform_version" {
    type = string
    description = "Platform version on which to run your service"
    default = "LATEST"
}

variable "scheduling_strategy" {
    type = string
    description = "Scheduling strategy to use for the service. The valid values are REPLICA and DAEMON"
    default = "REPLICA"
}

variable "service_connect_configuration" {
    type = map(any)
    description = "The ECS Service Connect configuration for this service to discover and connect to services, and be discovered by, and connected from, other services within a namespace"
    default = {}
}

variable "service_registries" {
    type = map(any)
    description = "Service discovery registries for the service"
    default = {}
}

variable "ecs_service_tags" {
    type = map(string)
    description = "Key-value map of resource tags."
    default = {}
}

variable "task_definition" {
    type = string
    description = "Family and revision (family:revision) or full ARN of the task definition that you want to run in your service."
    default = null
}

variable "triggers" {
    type = map(string)
    description = "Map of arbitrary keys and values that, when changed, will trigger an in-place update"
    default = {}
}

variable "wait_for_steady_state" {
    type = bool
    description = "If true, Terraform will wait for the service to reach a steady state (like aws ecs wait services-stable) before continuing"
    default = false 
}

variable "container_definitions" {
    type = string
    description = "A list of valid container definitions provided as a single valid JSON document."
    default = ""
}

variable "family" {
    type = string
    description = "A unique name for your task definition."
    default = null
}

variable "cpu" {
    type = string
    description = "Number of cpu units used by the task."
    default = null
}

variable "execution_role_arn" {
    type = string
    description = "ARN of the task execution role that the Amazon ECS container agent and the Docker daemon can assume"
    default = null
}

variable "inference_accelerator" {
    type = map(any)
    description = "Configuration block(s) with Inference Accelerators settings"
    default = {}
}

variable "ipc_mode" {
    type = string
    description = "IPC resource namespace to be used for the containers in the task"
    default = null
}

variable "memory" {
    type = number
    description = "Amount (in MiB) of memory used by the task"
    default = 0
}

variable "network_mode" {
    type = string
    description = "Docker networking mode to use for the containers in the task."
    default = "awsvpc"
}

variable "runtime_platform" {
    type = map(any)
    description = "Configuration block for runtime_platform that containers in your task may use."
    default = {}
}

variable "pid_mode" {
    type = string
    description = "Process namespace to use for the containers in the task."
    default = null
}

variable "task_placement_constraints" {
    type = map(any)
    description = " Configuration block for rules that are taken into consideration during task placement."
    default = {}
}

variable "proxy_configuration" {
    type = map(any)
    description = "Configuration block for the App Mesh proxy."
    default = {}
}

variable "ephemeral_storage" {
    type = map(any)
    description = "The amount of ephemeral storage to allocate for the task."
    default = {}
}

variable "requires_compatibilities" {
    type = list(string)
    description = "Set of launch types required by the task."
    default = null
}

variable "skip_destroy" {
    type = bool
    description = "Whether to retain the old revision when the resource is destroyed or replacement is necessary."
    default = false
}

variable "task_role_arn" {
    type = string
    description = "ARN of IAM role that allows your Amazon ECS container task to make calls to other AWS services."
    default = null
}

variable "volume" {
    type = map(any)
    description = "Configuration block for volumes that containers in your task may use."
    default = {}
}

#SErvice autoscaling
variable "create_autoscaling_fargate" {
    type = bool
    description = "Whether to create autoscaling Fargate service"
    default = false
}

variable "appautoscale_policy" {
    type = any
    description = "appautoscale for"
    default = []
}

variable "autoscale_metrics_name" {
    type = string
    description = "Name of the policy"
    default = "self-test"
}

variable "max_scale_capacity" {
    type = number
    description = "Max capacity of the scalable target"
    default = 2
}

variable "min_scale_capacity" {
    type = number
    description = "Min capacity of the scalable target"
    default = 1
}

variable "appautoscaling_role_arn" {
    type = string
    description = "ARN of the IAM role that allows Application AutoScaling to modify your scalable target on your behalf."
    default = ""
}

variable "policy_type" {
    type = string
    description = "Valid values are StepScaling and TargetTrackingScaling"
    default = "StepScaling"
}

variable "step_scaling_policy_configuration" {
    type = any
    description = "Step scaling policy configuration"
    default = []
}

variable "target_tracking_scaling_policy_configuration" {
    type = any
    description = "target_tracking_scaling_policy_configuration"
    default = []
}

variable "tags" {
    type = map(string)
    description = "tags for autoscaling"
    default = {
        NAME = "self-test"
    }
}