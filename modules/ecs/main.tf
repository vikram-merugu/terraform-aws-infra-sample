locals {
    name = "self-test"
    is_fargate = var.requires_compatibilities == "FARGATE" ? true : false
}
#cluster 
resource "aws_ecs_cluster" "this" {
    count = var.create_ecs_cluster ? 1 : 0
    name = var.cluster_name
    dynamic "configuration" {
        for_each = var.configuration
        content {
            dynamic "execute_command_configuration" {
                for_each = lookup(configuration.value, "execute_command_configuration", {})
                content {
                  kms_key_id = try(execute_command_configuration.value.kms_key_id, null)
                  logging = lookup(execute_command_configuration.value, "logging","DEFAULT")
                  dynamic "log_configuration" {
                    for_each = try(execute_command_configuration.value.logging,[]) 
                    content {
                        cloud_watch_encryption_enabled = lookup(log_configuration.value, "cloud_watch_encryption_enabled", false)
                        cloud_watch_log_group_name = lookup(log_configuration.value, "cloud_watch_log_group_name", null)
                        s3_bucket_name = lookup(log_configuration.value, "s3_bucket_name", null)
                        s3_bucket_encryption_enabled = lookup(log_configuration.value, "s3_bucket_encryption_enabled", false)
                        s3_key_prefix = lookup(log_configuration.value, "s3_key_prefix", null)
                    }
                  }
                }
        }
    }
    }
}
#cluster capacity provider

resource "aws_ecs_cluster_capacity_providers" "this" {
    count = length(var.capacity_providers) != 0 ? 1 : 0
    capacity_providers = var.capacity_providers
    cluster_name = aws_ecs_cluster.this[0].id
    dynamic "default_capacity_provider_strategy" {
        for_each = try(var.default_capacity_provider_strategy, [])
        content {
            capacity_provider = default_capacity_provider_strategy.value["capacity_provider"]
            weight = lookup(default_capacity_provider_strategy.value, "weight", null)
            base = lookup(default_capacity_provider_strategy.value, "base", null)
        }  
    } 
}

#ECS service

resource "aws_ecs_service" "this" {
    count = var.create_ecs_service ? 1 : 0
    name = var.service_name
    cluster = aws_ecs_cluster.this[0].arn

    dynamic "alarms" {
        for_each = length(var.alarms) > 0 ? [var.alarms] : []
        content {
         alarm_names = try(alarms.value.alarm_names, null)
         enable = try(alarms.value.enable, null)
         rollback = try(alarms.value.rollback, null)
        }
    }

    dynamic "capacity_provider_strategy" {
        for_each = length(var.capacity_provider_strategy) > 0 ? [var.capacity_provider_strategy] : []
        content {
          capacity_provider = try(capacity_provider_strategy.value.capacity_provider, null)
          weight = try(capacity_provider_strategy.value.weight, null)
          base = try(capacity_provider_strategy.value.base, null)
        }
    }
    
    dynamic "deployment_circuit_breaker" {
        for_each = length(var.deployment_circuit_breaker) > 0 ? [var.deployment_circuit_breaker] : []
        content {
            enable = try(deployment_circuit_breaker.value.enable,false)
            rollback = try(deployment_circuit_breaker.value.rollback,null)
        }
    }

    dynamic "deployment_controller" {
        for_each = length(var.deployment_controller) > 0 ? [var.deployment_controller] : []
        content {
            type = try(deployment_controller.value.type, null)
        }
    }
    deployment_maximum_percent = local.is_fargate ? var.deployment_maximum_percent : 0
    deployment_minimum_healthy_percent = var.deployment_minimum_healthy_percent
    desired_count = local.is_fargate ? var.desired_count : 0
    enable_ecs_managed_tags = !local.is_fargate ? var.enable_ecs_managed_tags : false
    enable_execute_command = var.enable_execute_command
    force_new_deployment = var.force_new_deployment
    health_check_grace_period_seconds = var.health_check_grace_period_seconds 
    iam_role = !local.is_fargate ? var.iam_role : null
    launch_type = var.launch_type 
    dynamic "load_balancer" {
        for_each = length(var.load_balancer) > 0 ? [var.load_balancer] : []
        content {
            elb_name = try(load_balancer.value.elb_name, null)
            target_group_arn = try(load_balancer.value.target_group_arn, null)
            container_name = try(load_balancer.value.container_name, null)
            container_port = try(load_balancer.value.container_port, null)
        }
    }
    dynamic "network_configuration" {
        for_each = length(var.network_configuration) > 0 ? [var.network_configuration] : []
        content {
            subnets = try(network_configuration.value.subnets, null)
            security_groups = try(network_configuration.value.security_groups, [])
            assign_public_ip = try(network_configuration.value.assign_public_ip, false)
        }
    }

    dynamic "ordered_placement_strategy" {
        for_each = length(var.ordered_placement_strategy) > 0 ? [var.ordered_placement_strategy] : []
        content {
            type = try(ordered_placement_strategy.value.type, null)
            field = try(ordered_placement_strategy.value.field, null)
        }
    }
    dynamic "placement_constraints" {
        for_each = length(var.task_placement_constraints) > 0 ? [var.task_placement_constraints] : []
        content {
            type = try(placement_constraints.value.type, null)
            expression = try(placement_constraints.value.expression, null)
        }
    }
    platform_version = var.platform_version
    scheduling_strategy = var.scheduling_strategy
    dynamic "service_connect_configuration" {
        for_each = length(var.service_connect_configuration) > 0 ? [var.service_connect_configuration] : []
        content {
            enabled = try(service_connect_configuration.value.enabled, null)
             dynamic "log_configuration" {
                for_each = try([service_connect_configuration.value.log_configuration], [])
                content {
                    log_driver = try(log_configuration.value.log_driver, null)
                    options = try(log_configuration.value.options, null)
                    dynamic "secret_option" {
                        for_each = try([log_configuration.value.secret_option], []) 
                        content {
                            name = secret_option.value.name
                            value_from = secret_option.value.value_from
                        }
                    }
                }
        }
    }
    }
    dynamic "service_registries" {
        for_each = length(var.service_registries) > 0 ? [var.service_registries] : []
        content {
            registry_arn = service_registries.value.registry_arn 
            port = try(service_registries.value.port, null)
            container_port = try(service_registries.value.container_port, null)
            container_name = try(service_registries.value.container_name, null)
        }
    } 
    tags = var.ecs_service_tags
    task_definition = aws_ecs_task_definition.this[0].arn
    triggers = var.triggers
    wait_for_steady_state = var.wait_for_steady_state
    depends_on = [aws_ecs_task_definition.this]
}


#ECS Task definition

resource "aws_ecs_task_definition" "this" {
    count = var.create_ecs_cluster && (length(var.container_definitions) != 0) ? 1 : 0
    container_definitions = var.container_definitions
    family = var.family
    cpu = local.is_fargate ? var.cpu : null
    execution_role_arn = var.execution_role_arn
    dynamic "inference_accelerator" {
        for_each = length(var.inference_accelerator) > 0 ? [var.inference_accelerator] : []
        content {
            device_name = inference_accelerator.value.device_name
            device_type = inference_accelerator.value.device_type
        }
    }
    ipc_mode = var.ipc_mode
    memory = local.is_fargate ? var.memory : null
    network_mode = var.network_mode
    dynamic "runtime_platform" {
        for_each = length(var.runtime_platform) > 0 ? [var.runtime_platform] : []
        content {
            operating_system_family = try(runtime_platform.value.operating_system_family, null)
            cpu_architecture = try(runtime_platform.value.cpu_architecture, null)
        }
    }
    pid_mode = var.pid_mode
    dynamic "placement_constraints" {
        for_each = length(var.placement_constraints) > 0 ? [var.placement_constraints] : []
        content {
            expression = try(placement_constraints.value.expression, null)
            type = placement_constraints.value.type
        }
    }
    dynamic "proxy_configuration" {
        for_each = length(var.proxy_configuration) > 0 ? [var.proxy_configuration] : []
        content {
            container_name = proxy_configuration.value.container_name
            properties = proxy_configuration.value.properties
            type = try(proxy_configuration.value.type, null)
        }
    }
    dynamic "ephemeral_storage" {
        for_each = length(var.ephemeral_storage) > 0 ? [var.ephemeral_storage] : []
        content {
            size_in_gib = ephemeral_storage.value.size_in_gib
        }
    }
    requires_compatibilities = var.requires_compatibilities
    skip_destroy = var.skip_destroy
    tags = var.ecs_service_tags
    task_role_arn = var.task_role_arn
    dynamic "volume" {
        for_each = length(var.volume) > 0 ? [var.volume] :[]
        content {
            dynamic "docker_volume_configuration" {
                for_each = try([volume.value.docker_volume_configuration], [])
                    content {
                    autoprovision = try(docker_volume_configuration.value.autoprovision, null)
                    driver_opts = try(docker_volume_configuration.value.driver_opts, null)
                    driver = try(docker_volume_configuration.value.driver, null)
                    labels = try(docker_volume_configuration.value.labels, null)
                    scope = try(docker_volume_configuration.value.scope, null)
                }
            }
            dynamic "efs_volume_configuration" {
                for_each = try([volume.value.efs_volume_configuration], [])
                content {
                    file_system_id = efs_volume_configuration.value.file_system_id
                    root_directory = try(efs_volume_configuration.value.root_directory, null)
                    transit_encryption = try(efs_volume_configuration.value.transit_encryption, null)
                    transit_encryption_port = try(efs_volume_configuration.value.transit_encryption_port, null)
                    dynamic "authorization_config" {
                        for_each = try([efs_volume_configuration.value.authorization_config], [])
                        content {
                            access_point_id = try(authorization_config.value.access_point_id, null)
                            iam = try(authorization_config.value.iam, null)
                        } 
                    } 
                }
            }
            dynamic "fsx_windows_file_server_volume_configuration" {
                for_each = try([volume.value.fsx_windows_file_server_volume_configuration], [])
                iterator = i
                content {
                    file_system_id = i.value.file_system_id
                    root_directory = i.value.root_directory
                    dynamic "authorization_config" {
                        for_each = try([i.value.authorization_config], [])
                        content {
                            credentials_parameter = authorization_config.value.credentials_parameter
                            domain = authorization_config.value.domain 
                        }
                    }
                }
            }
            host_path = try(volume.value.host_path, null)
            name = volume.value.name
        }
    }

}
#Autoscaling for service
resource "aws_appautoscaling_target" "this" {
    count = var.create_ecs_service && var.create_autoscaling_fargate ? 1 : 0
    max_capacity = var.max_scale_capacity
    min_capacity = var.min_scale_capacity
    resource_id = "service/${aws_ecs_cluster.this[0].name}/${aws_ecs_service.this[0].name}"
    role_arn = var.appautoscaling_role_arn
    scalable_dimension = "ecs:service:DesiredCount"
    service_namespace = "ecs"
    tags = var.tags
}

#Autoscaling policy
resource "aws_appautoscaling_policy" "this" {
    count = var.create_ecs_service && var.create_autoscaling_fargate ? length(var.appautoscale_policy) : 0
    name = var.autoscale_metrics_name
    policy_type = var.policy_type
    resource_id = aws_appautoscaling_target.this[0].resource_id
    scalable_dimension = aws_appautoscaling_target.this[0].scalable_dimension
    service_namespace = aws_appautoscaling_target.this[0].service_namespace
    dynamic "step_scaling_policy_configuration" {
        for_each = length(var.step_scaling_policy_configuration) > 0 ? var.step_scaling_policy_configuration : []
        content {
            adjustment_type = step_scaling_policy_configuration.value.adjustment_type
            cooldown = step_scaling_policy_configuration.value.cooldown
            metric_aggregation_type = try(step_scaling_policy_configuration.value.metric_aggregation_type, "Average")
            min_adjustment_magnitude = try(step_scaling_policy_configuration.value.min_adjustment_magnitude, "")
            dynamic "step_adjustment" {
                for_each = length(keys(try(step_scaling_policy_configuration.value.step_adjustment, ""))) > 0 ? [step_scaling_policy_configuration.value.step_adjustment] : []
                content {
                    metric_interval_lower_bound = try(step_adjustment.value.metric_interval_lower_bound, null)
                    metric_interval_upper_bound = try(step_adjustment.value.metric_interval_upper_bound, null)
                    scaling_adjustment  = step_adjustment.value.scaling_adjustment 
                }
            }
        }
    }
    dynamic "target_tracking_scaling_policy_configuration" {
        for_each = length(var.target_tracking_scaling_policy_configuration) > 0 ? [var.target_tracking_scaling_policy_configuration] : []
        content{
            target_value = target_tracking_scaling_policy_configuration.value.target_value
            disable_scale_in = try(target_tracking_scaling_policy_configuration.value.disable_scale_in, false)
            scale_in_cooldown = try(target_tracking_scaling_policy_configuration.value.scale_in_cooldown, 60)
            scale_out_cooldown = try(target_tracking_scaling_policy_configuration.value.scale_out_cooldown, 60)
            dynamic "predefined_metric_specification" {
                for_each = length(target_tracking_scaling_policy_configuration.value.predefined_metric_specification) > 0 ? [target_tracking_scaling_policy_configuration.value.predefined_metric_specification] : [] 
                content {
                    predefined_metric_type = predefined_metric_specification.value.predefined_metric_type
                }
            }
        }
    }
}
