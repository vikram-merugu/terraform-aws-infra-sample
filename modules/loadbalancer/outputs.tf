output "load_balancer_arn" {
  description = "The ARN of the load balancer"
  value = try(aws_lb.this[0].arn, "")
}

output "load_balancer_id" {
    description = "The ID of the load balancer"
    value = try(aws_lb.this[0].id, "")
}

output "load_balancer_dns_name" {
    description = "The DNS name of the load balancer."
    value = try(aws_lb.this[0].dns_name, "")
}

#listener outputs
output "load_balancer_listener_arn" {
  description = "The ARN of the load balancer listener"
  value = try(aws_lb_listener.https_listener.*.arn, "")
}

output "load_balancer_listener_id" {
    description = "The ID of the load balancer listener"
    value = try(aws_lb_listener.https_listener.*.id, "")
}

#target_group
output "target_group_arn" {
  description = "ARN of the Target Group"
  value = try(aws_lb_target_group.this.*.arn, "")
}

output "target_group_id" {
  description = "ID of the Target Group"
  value = try(aws_lb_target_group.this.*.id, "")
}
