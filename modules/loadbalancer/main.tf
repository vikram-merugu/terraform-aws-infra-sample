locals {
    load_balancer_name = "${var.name}-${var.name_suffix}"
    target_group_attachments = merge(flatten([
    for index, group in var.target_groups : [
      for k, targets in group : {
        for target_key, target in targets : join(".", [index, target_key]) => merge({ tg_index = index }, target)
      }
      if k == "targets"
    ]
  ])...)

}


#creation of load balancer
resource "aws_lb" "this" {
    count = var.create_load_balancer ? 1 : 0
    dynamic "access_logs" {
        for_each = length(keys(var.access_logs)) == 0 ? {} : var.access_logs
        content {
        bucket = try(access_logs.value.bucket, null)
        enabled = try(access_logs.value.enabled, false)
        prefix = try(access.logs.value.prefix, null)
    }
    }
    enable_cross_zone_load_balancing = var.enable_cross_zone_lb
    enable_deletion_protection = var.enable_deletion_protection
    enable_http2 = var.enable_http2
    internal = var.internal
    ip_address_type = var.ip_address_type
    load_balancer_type = var.load_balancer_type
    name = local.load_balancer_name
    name_prefix = var.name_prefix
    security_groups = var.security_group_ids
    preserve_host_header = var.preserve_host_header
    subnets = var.subnet_ids
    xff_header_processing_mode = var.xff_header_processing_mode
    dynamic "subnet_mapping" {
        for_each = var.subnet_mapping
        content {
          subnet_id = try(subnet_mapping.value.subnet_id, "")
          allocation_id = try(subnet_mapping.value.allocation_id, "")
          private_ipv4_address = try(subnet_mapping.value.private_ipv4_address, "")
        }

      
    }
}

#creation of https listener
resource "aws_lb_listener" "https_listener" {
    count = var.create_load_balancer && (length(var.https_listener) > 0) ? length(var.https_listener) : 0
    load_balancer_arn = aws_lb.this[0].arn
    dynamic "default_action" {
      for_each = length(keys(var.https_listener[count.index])) != 0 ? [var.https_listener[count.index]] : []
      content {
        type = default_action.value.type
        dynamic "fixed_response" {
            for_each = length(keys(try(default_action.value.fixed_response, null))) > 0 ? [default_action.value.fixed_response] : []
            content {
                content_type = fixed_response.value.content_type
                message_body = try(fixed_response.value.message_body, null)
                status_code = try(fixed_response.value.status_code, null)
            }
        }
        dynamic "forward" {
            for_each = length(keys(lookup(default_action.value,"forward", {}))) > 0 ? [default_action.value.forward] : []
            content {
              dynamic "target_group" {
                for_each = length(keys(try(forward.value.target_group, {}))) > 0 ? [forward.value.target_group] : []
                content {
                    arn = target_group.value.arn
                    weight = try(target_group.value.weight, null)
                } 
              }
              dynamic "stickiness" {
                for_each = length(keys(forward.value.stickness)) > 0 ? [forward.value.stickness] : []
                content {
                     duration = stickness.value.duration
                     enabled = try(stickness.value.enabled, false)
                }
              }
            }
        }
        dynamic "redirect"{
            for_each = length(keys(try(default_action.value.redirect, {}))) > 0 ? [default_action.value.redirect] : []
            content {
              status_code = redirect.value.status_code
              host = lookup(redirect.value, "host", null)
              path = lookup(redirect.value, "path", null)
              port = lookup(redirect.value, "port", null)
              protocol = lookup(redirect.value, "protocol", null)
              query = lookup(redirect.value, "query", null)
            }
        }
        target_group_arn = try(default_action.value.target_action_arn, null)
      }
    }
    certificate_arn = try(var.https_listener[count.index]["certificate_arn"], null)
    port = var.https_listener[count.index]["port"]
    protocol = var.https_listener[count.index]["protocol"]
    ssl_policy = try(var.https_listener[count.index]["ssl_policy"], null)
    tags = var.tags
}

#listener rule

resource "aws_lb_listener_rule" "https_listener_rules" {
    count = var.create_load_balancer && (length(var.https_listener_rules) > 0) ? length(var.https_listener_rules) : 0
    listener_arn = aws_lb_listener.https_listener[lookup(var.https_listener[count.index], "https_listener_index",count.index)].arn
    priority = var.https_listener_rules[count.index]["priority"]

    #forward
    dynamic "action" {
    for_each = [for action_rule in var.https_listener_rules[count.index].actions :
      action_rule if action_rule.type == "forward"]
        content {
          type = action.value.type
          target_group_arn = try(action.value.target_group_arn, null)
          forward {
            dynamic "target_group" {
              for_each = action.value["target_groups"]
              #length(keys(try(forward.value.target_group, null))) > 0 ? [forward.value.target_group] : [] 
              content {
                arn = aws_lb_target_group.main[target_group.value["target_group_index"]].id
                weight = lookup(target_group.value, "weight", 0)
              }
            }
            dynamic "stickiness" {
              for_each = [lookup(action.value, "stickiness", {})]
              #length(keys(try(forward.value.stickiness, null))) > 0 ? [forward.value.stickiness] : []
              content{
                enabled = lookup(stickiness.value, "enabled", false)
                duration = lookup(stickiness.value, "duration", null)
              }
         }
    }
        }
    }
    
    #redirect
    dynamic "action" {
    for_each = [
      for action_rule in var.https_listener_rules[count.index].actions :
      action_rule if action_rule.type == "redirect"]
      content {
        type = action.value.type
        redirect {
          host = lookup(action.value, "host",null)
          path = lookup(action.value, "path", null)
          port = lookup(action.value, "port", null)
          protocol = lookup(action.value, "protocol", null)
          query = lookup(action.value, "query", null)
          status_code = action.value.status_code
        }
      }
    }
    
    #fixed response
    dynamic "action" {
            for_each = [for action_rule in var.https_listener_rules[count.index].actions :
      action_rule if action_rule.type == "fixed-response"]
            content {
              type = action.value.type
              fixed_response {
                content_type = action.value.content_type
                message_body = lookup(action.value, "message_body", null)
                status_code = lookup(action.value, "status_code", null)
                }
            }
          }
        
    #host headers
    dynamic "condition" {
        for_each = [for condition_rule in var.https_listener_rules[count.index].conditions :
      condition_rule if length(lookup(condition_rule,"host_header", [])) > 0 ]
        content {
            host_header {
                values = [condition.value["host_header"]]
            }
        }
    }
    #http headers
    dynamic "condition" {
        for_each = [for condition_rule in var.https_listener_rules[count.index].conditions :
      condition_rule if length(lookup(condition_rule,"http_header",[])) > 0 ]
        content {
            dynamic "http_header" {
              for_each = condition.value["http_header"]
              content {
               http_header_name = http_header.value.http_header_name
               values = http_header.value.values 
              } 
            }
        }
    }
    #query string
   dynamic "condition" {
    for_each = [for condition_rule in var.https_listener_rules[count.index].conditions :
      condition_rule if length(lookup(condition_rule,"query_string", [])) > 0 ]
    content {
      query_string {
        values = condition.values.query_string
      }
    }
   } 
   dynamic "condition" {
    for_each = [for condition_rule in var.https_listener_rules[count.index].conditions :
      condition_rule if length(lookup(condition_rule,"host_header", [])) > 0 ]
    content {
      source_ip {
        value = condition.value.source_ip
      }
    }
   }
    tags = var.tags
}

#creation of http listener
resource "aws_lb_listener" "http_listener" {
    count = var.create_load_balancer &&length(var.http_listener) > 0 ? length(var.http_listener) : 0
    load_balancer_arn = aws_lb.this[0].arn
    dynamic "default_action" {
      for_each = length(keys(var.http_listener[count.index])) != 0 ? [var.http_listener[count.index]] : []
      content {
        type = default_action.value.type
        dynamic "fixed_response" {
            for_each = length(keys(try(default_action.value.fixed_response, null))) > 0 ? [default_action.value.fixed_response] : []
            content {
                content_type = fixed_response.value.content_type
                message_body = try(fixed_response.value.message_body, null)
                status_code = try(fixed_response.value.status_code, null)
            }
        }
        dynamic "forward" {
            for_each = length(keys(lookup(default_action.value,"forward", {}))) > 0 ? [default_action.value.forward] : []
            content {
              dynamic "target_group" {
                for_each = length(keys(try(forward.value.target_group, {}))) > 0 ? [forward.value.target_group] : []
                content {
                    arn = target_group.value.arn
                    weight = try(target_group.value.weight, null)
                } 
              }
              dynamic "stickiness" {
                for_each = length(keys(forward.value.stickness)) > 0 ? [forward.value.stickness] : []
                content {
                     duration = stickness.value.duration
                     enabled = try(stickness.value.enabled, false)
                }
              }
            }
        }
        dynamic "redirect"{
            for_each = length(keys(try(default_action.value.redirect, {}))) > 0 ? [default_action.value.redirect] : []
            content {
              status_code = redirect.value.status_code
              host = lookup(redirect.value, "host", null)
              path = lookup(redirect.value, "path", null)
              port = lookup(redirect.value, "port", null)
              protocol = lookup(redirect.value, "protocol", null)
              query = lookup(redirect.value, "query", null)
            }
        }
        target_group_arn = try(default_action.value.target_action_arn, null)
      }
    }
    #certificate_arn = try(var.http_listener[count.index]["certificate_arn"], null)
    port = var.http_listener[count.index]["port"]
    protocol = var.http_listener[count.index]["protocol"]
    #ssl_policy = try(var.http_listener[count.index]["ssl_policy"], null)
    tags = var.tags
}

#listener rule

resource "aws_lb_listener_rule" "http_listener_rules" {
    count = var.create_load_balancer && (length(var.http_listener_rules) > 0) ? length(var.http_listener_rules) : 0
    listener_arn = aws_lb_listener.http_listener[lookup(var.http_listener[count.index], "http_listener_index",count.index)].arn
    priority = var.http_listener_rules[count.index]["priority"]

    #forward
    dynamic "action" {
    for_each = [for action_rule in var.http_listener_rules[count.index].actions :
      action_rule if action_rule.type == "forward"]
        content {
          type = action.value.type
          target_group_arn = try(action.value.target_group_arn, null)
          forward {
            dynamic "target_group" {
              for_each = action.value["target_groups"]
              #length(keys(try(forward.value.target_group, null))) > 0 ? [forward.value.target_group] : [] 
              content {
                arn = aws_lb_target_group.main[target_group.value["target_group_index"]].id
                weight = lookup(target_group.value, "weight", 0)
              }
            }
            dynamic "stickiness" {
              for_each = [lookup(action.value, "stickiness", {})]
              #length(keys(try(forward.value.stickiness, null))) > 0 ? [forward.value.stickiness] : []
              content{
                enabled = lookup(stickiness.value, "enabled", false)
                duration = lookup(stickiness.value, "duration", null)
              }
         }
    }
        }
    }
    
    #redirect
    dynamic "action" {
    for_each = [
      for action_rule in var.http_listener_rules[count.index].actions :
      action_rule if action_rule.type == "redirect"]
      content {
        type = action.value.type
        redirect {
          host = lookup(action.value, "host",null)
          path = lookup(action.value, "path", null)
          port = lookup(action.value, "port", null)
          protocol = lookup(action.value, "protocol", null)
          query = lookup(action.value, "query", null)
          status_code = action.value.status_code
        }
      }
    }
    
    #fixed response
    dynamic "action" {
            for_each = [for action_rule in var.http_listener_rules[count.index].actions :
      action_rule if action_rule.type == "fixed-response"]
            content {
              type = action.value.type
              fixed_response {
                content_type = action.value.content_type
                message_body = lookup(action.value, "message_body", null)
                status_code = lookup(action.value, "status_code", null)
                }
            }
          }
        
    #host headers
    dynamic "condition" {
        for_each = [for condition_rule in var.http_listener_rules[count.index].conditions :
      condition_rule if length(lookup(condition_rule,"host_header", [])) > 0 ]
        content {
            host_header {
                values = [condition.value["host_header"]]
            }
        }
    }
    #http headers
    dynamic "condition" {
        for_each = [for condition_rule in var.http_listener_rules[count.index].conditions :
      condition_rule if length(lookup(condition_rule,"http_header",[])) > 0 ]
        content {
            dynamic "http_header" {
              for_each = condition.value["http_header"]
              content {
               http_header_name = http_header.value.http_header_name
               values = http_header.value.values 
              } 
            }
        }
    }
    #query string
   dynamic "condition" {
    for_each = [for condition_rule in var.http_listener_rules[count.index].conditions :
      condition_rule if length(lookup(condition_rule,"query_string", [])) > 0 ]
    content {
      query_string {
        values = condition.values.query_string
      }
    }
   } 
   dynamic "condition" {
    for_each = [for condition_rule in var.http_listener_rules[count.index].conditions :
      condition_rule if length(lookup(condition_rule,"host_header", [])) > 0 ]
    content {
      source_ip {
        value = condition.value.source_ip
      }
    }
   }
    tags = var.tags
}


#creation of target_group

resource "aws_lb_target_group" "this" {
  count = length(var.target_groups) > 0 ? length(var.target_groups) : 0

  deregistration_delay = var.deregistration_delay
  dynamic "health_check" {
    for_each = length(keys(lookup(var.target_groups[count.index], "health_check"))) > 0 ? [lookup(var.target_groups[count.index], "health_check", {})] : []
    content {
      enabled = lookup(health_check.value, "enabled", true)
      healthy_threshold = lookup(health_check.value, "healthy_threshold", 3)
      interval = lookup(health_check.value, "interval", 30)
      matcher = lookup(health_check.value, "matcher", null)
      path = lookup(health_check.value, "path", null)
      port = lookup(health_check.value, "port", null)
      protocol = lookup(health_check.value, "protocol", null)
      timeout = lookup(health_check.value, "timeout", 30)
      unhealthy_threshold = lookup(health_check.value, "unhealthy_threshold", 3)
    }
  }
  load_balancing_cross_zone_enabled = lookup(var.target_groups[count.index], "load_balancing_cross_zone_enabled", false)
  name_prefix = lookup(var.target_groups[count.index], "name_prefix", null)
  name = lookup(var.target_groups[count.index], "name", null)  
  port = lookup(var.target_groups[count.index], "port", null)
  protocol = lookup(var.target_groups[count.index], "protocol", null)
  tags = var.tags
  target_type = lookup(var.target_groups[count.index], "target_type", null)
  ip_address_type = var.ip_address_type
  vpc_id = var.vpc_id
  dynamic "stickiness" {
    for_each = length(keys(lookup(var.target_groups[count.index], "stickiness", {}))) > 0 ? [lookup(var.target_groups[count.index], "stickiness", {})] : []
    content {
      cookie_duration = lookup(stickiness.value, "cookie_duration", 1)
      cookie_name = lookup(stickiness.value, "cookie_name", null)
      enabled = lookup(stickiness.value, "enabled", "true")
      type = lookup(stickiness.value, "type", null)
    }
  }
}

#target_group_attachment
resource "aws_lb_target_group_attachment" "this" {
  for_each = { for k, v in local.target_group_attachments : k => v if var.create_load_balancer }

  target_group_arn  = aws_lb_target_group.this[each.value.tg_index].arn
  target_id         = each.value.target_id
  port              = lookup(each.value, "port", null)
  availability_zone = lookup(each.value, "availability_zone", null)
}