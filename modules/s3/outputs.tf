output "s3_bucket_id" {
  description = "The name of the bucket."
  value       = try(aws_s3_bucket.this[0].id, "")
}

output "s3_bucket_arn" {
  description = "The ARN of the bucket. Will be of format arn:aws:s3:::bucketname."
  value       = try(aws_s3_bucket.this[0].arn, "")
}

output "s3_bucket_bucket_domain_name" {
  description = "The bucket domain name. Will be of format bucketname.s3.amazonaws.com."
  value       = try(aws_s3_bucket.this[0].bucket_domain_name, "")
}

output "s3_bucket_bucket_regional_domain_name" {
  description = "The bucket region-specific domain name. The bucket domain name including the region name, please refer here for format. Note: The AWS CloudFront allows specifying S3 region-specific endpoint when creating S3 origin, it will prevent redirect issues from CloudFront to S3 Origin URL."
  value       = try(aws_s3_bucket.this[0].bucket_regional_domain_name, "")
}

output "s3_bucket_region" {
  description = "The AWS region this bucket resides in."
  value       = try(aws_s3_bucket.this[0].region, "")
}

output "s3_bucket_website_domain" {
  description = "The domain of the website endpoint, if the bucket is configured with a website. If not, this will be an empty string. This is used to create Route 53 alias records."
  value       = try(aws_s3_bucket_website_configuration.this[0].website_domain, "")
}

output "cloudfront_access_identity_path" {
  description = "A shortcut to the full path for the origin access identity to use in CloudFront"
  value       = try(aws_cloudfront_origin_access_identity.this[0].cloudfront_access_identity_path, "")
}

output "cloudfront_id" {
  description = "The identifier for the distribution. For example: EDFDVBD632BHDS5."
  value       = try(aws_cloudfront_origin_access_identity.this[0].id, "")
}

output "cloudfront_iam_arn" {
  description = "A pre-generated ARN for use in S3 bucket policies (see below). Example: arn:aws:iam::cloudfront:user/CloudFront Origin Access Identity E2QWRUHAPOMQZL."
  value       = try(aws_cloudfront_origin_access_identity.this[0].iam_arn, [])
}
