# --------------------------------------------------------------------
# Creation of local variable
# --------------------------------------------------------------------
locals {
  cross_account_bucket_actions_enabled                 = local.cross_account_enabled && length(var.cross_account_bucket_actions) > 0
  cross_account_object_actions_enabled                 = local.cross_account_enabled && length(var.cross_account_object_actions) > 0
  cross_account_object_actions_with_forced_acl_enabled = local.cross_account_enabled && length(var.cross_account_object_actions_with_forced_acl) > 0

  cross_account_actions_enabled = local.cross_account_bucket_actions_enabled || local.cross_account_object_actions_enabled || local.cross_account_object_actions_with_forced_acl_enabled

  origin_access_identities_enabled = local.create_bucket && (var.create_origin_access_identity || length(var.origin_access_identities) > 0)
  oai_identities = concat(
    var.origin_access_identities,
    try([aws_cloudfront_origin_access_identity.this[0].iam_arn], [])
  )

  policy_enabled = var.create_bucket && (local.cross_account_actions_enabled || local.origin_access_identities_enabled)
}

# --------------------------------------------------------------------
# Bucket policy for S3 read permissions when using bucket as website
# --------------------------------------------------------------------
data "aws_iam_policy_document" "s3_read_permissions" {
  /*
        This data provides the nessesary policy for accesing a website bucket
    :param s3_bucket_name
  */
  count = local.create_bucket && var.attach_public_policy ? 1 : 0

  statement {
    sid = "PublicReadGetObject"

    principals {
      type        = "*"
      identifiers = ["*"]
    }

    actions = ["s3:GetObject"]

    resources = ["arn:aws:s3:::${local.s3_bucket_name}/*"]
  }
}

# --------------------------------------------------------------------
# Attach a Policy to the S3 Bucket to control:
# - Cross account bucket actions
# - Cross account object actions
# - Cloudfront origin access identity access
# --------------------------------------------------------------------
data "aws_iam_policy_document" "cors_oai_policy" {
  /*
    :param create_bucket
    :param policy_enabled
    :param cross_account_bucket_actions_enabled
    :param cross_account_bucket_actions
    :param cross_account_identifiers
    :param cross_account_object_actions_enabled
    :param cross_account_object_actions
    :param cross_account_object_actions_with_forced_acl_enabled
    :param cross_account_object_actions_with_forced_acl
    :param cross_account_forced_acls
    :param origin_access_identities_enabled
    :param bucket_arn
    :param oai_identities
  */
  count = local.create_bucket && local.policy_enabled ? 1 : 0

  dynamic "statement" {
    for_each = local.cross_account_bucket_actions_enabled ? [1] : []

    content {
      actions   = var.cross_account_bucket_actions
      resources = [local.bucket_arn]

      principals {
        type        = "AWS"
        identifiers = var.cross_account_identifiers
      }
    }
  }

  dynamic "statement" {
    for_each = local.cross_account_object_actions_enabled ? [1] : []

    content {
      actions   = var.cross_account_object_actions
      resources = ["${local.bucket_arn}/*"]

      principals {
        type        = "AWS"
        identifiers = var.cross_account_identifiers
      }
    }
  }

  dynamic "statement" {
    for_each = local.cross_account_object_actions_with_forced_acl_enabled ? [1] : []

    content {
      actions   = var.cross_account_object_actions_with_forced_acl
      resources = ["${local.bucket_arn}/*"]

      principals {
        type        = "AWS"
        identifiers = var.cross_account_identifiers
      }

      dynamic "condition" {
        for_each = length(var.cross_account_forced_acls) == 0 ? [] : [1]

        content {
          test     = "StringEquals"
          variable = "s3:x-amz-acl"
          values   = var.cross_account_forced_acls
        }
      }
    }
  }

  dynamic "statement" {
    for_each = local.origin_access_identities_enabled ? [{
      actions   = ["s3:GetObject"]
      resources = ["${local.bucket_arn}/*"]
      }, {
      actions   = ["s3:ListBucket"]
      resources = [local.bucket_arn]

    }] : []
    content {
      actions   = statement.value.actions
      resources = statement.value.resources

      principals {
        type        = "AWS"
        identifiers = local.oai_identities
      }
    }
  }
}

# --------------------------------------------------------------------
# Bucket policy for S3 flow log
# --------------------------------------------------------------------
data "aws_iam_policy_document" "s3_flow_log_policy" {
  /*
        This data creates a policy for vpc flow logs
    :param create_bucket
    :param attach_policy_flow_logs
    :param s3_bucket_name
  */
  count = local.create_bucket && var.attach_policy_flow_logs ? 1 : 0
  statement {
    sid = "AWSLogDeliveryWrite"

    principals {
      type        = "Service"
      identifiers = ["delivery.logs.amazonaws.com"]
    }

    actions = ["s3:PutObject"]

    resources = ["arn:aws:s3:::${local.s3_bucket_name}/AWSLogs/*"]
  }

  statement {
    sid = "AWSLogDeliveryAclCheck"

    principals {
      type        = "Service"
      identifiers = ["delivery.logs.amazonaws.com"]
    }

    actions = ["s3:GetBucketAcl"]

    resources = ["arn:aws:s3:::${local.s3_bucket_name}"]
  }
}
