################################################################################
# Creation of KMS variables
################################################################################
variable "create_kms_key" {
  description = "Controls if KMS key should be created"
  type        = bool
  default     = false
}
variable "deletion_window_in_days" {
  description = "The waiting period, specified in number of days. After the waiting period ends, AWS KMS deletes the KMS key"
  type        = number
  default     = null
}

variable "enable_key_rotation" {
  description = "Enable key rotation for company rules"
  type        = bool
  default     = false
}

variable "kms_alias" {
  description = "The alias of the KMS key"
  type        = string
  default     = ""
}

################################################################################
# Creation of S3 variables
################################################################################
variable "create_bucket" {
  description = "Controls if a S3 bucket should be created"
  type        = bool
  default     = false
}

variable "create_website_bucket" {
  description = "Controls if a S3 website bucket should be created"
  type        = bool
  default     = false
}

variable "name" {
  description = "Name to be used on all the resources as identifier"
  type        = string
  default     = ""
}

variable "suffix_name" {
  description = "Suffix to be used on the S3 resource as identifier"
  type        = string
  default     = ""
}

variable "attach_policy_custom" {
  description = "Controls if S3 bucket should have bucket policy attached (set to `true` to use value of `policy` as bucket policy)"
  type        = bool
  default     = false
}

variable "policy_custom" {
  description = "(Optional) A valid bucket policy JSON document. Note that if the policy document is not specific enough (but still valid), Terraform may view the policy as constantly changing in a terraform plan. In this case, please make sure you use the verbose/specific version of the policy. For more information about building AWS IAM policy documents with Terraform, see the AWS IAM Policy Document Guide."
  type        = string
  default     = null
}

variable "force_destroy" {
  description = "(Optional, Default:false ) A boolean that indicates all objects should be deleted from the bucket so that the bucket can be destroyed without error. These objects are not recoverable."
  type        = bool
  default     = false
}

variable "object_lock_enabled" {
  description = "Whether S3 bucket should have an Object Lock configuration enabled."
  type        = bool
  default     = false
}

variable "tags" {
  description = "(Optional) A mapping of tags to assign to the bucket."
  type        = map(string)
  default     = {}
}

variable "versioning" {
  description = "Map containing versioning configuration."
  type        = map(string)
  default     = {}
}

variable "server_side_encryption_configuration" {
  description = "Map containing server-side encryption configuration."
  type        = any
  default     = {}
}

variable "website" {
  description = "Map containing static web-site hosting or redirect configuration."
  type        = map(string)
  default     = {}
}

variable "block_public_acls" {
  description = "Whether Amazon S3 should block public ACLs for this bucket."
  type        = bool
  default     = true
}

variable "block_public_policy" {
  description = "Whether Amazon S3 should block public bucket policies for this bucket."
  type        = bool
  default     = true
}

variable "ignore_public_acls" {
  description = "Whether Amazon S3 should ignore public ACLs for this bucket."
  type        = bool
  default     = true
}

variable "restrict_public_buckets" {
  description = "Whether Amazon S3 should restrict public bucket policies for this bucket."
  type        = bool
  default     = true
}

variable "attach_public_policy" {
  description = "Controls if a user defined public bucket policy will be attached (set to `false` to allow upstream to apply defaults to the bucket)"
  type        = bool
  default     = false
}

variable "lifecycle_rules" {
  description = "List of maps containing configuration of object lifecycle management."
  type        = any
  default     = []
}

variable "cors_rule" {
  description = "Object containing a rule of Cross-Origin Resource Sharing."
  type        = any
  default     = {}
}

variable "attach_policy_cors" {
  description = "Whether to attach a policy for cors, cross account object or bucket"
  type        = bool
  default     = false
}

variable "attach_policy_flow_logs" {
  description = "Whether to attach a policy for vpc flow logs"
  type        = bool
  default     = false
}

variable "cross_account_identifiers" {
  type        = list(string)
  description = "Identifiers that you want to grant cross account access to"
  default     = []
}

variable "cross_account_bucket_actions" {
  type        = list(string)
  description = "Actions on the bucket to allow"
  default = [
    "s3:ListBucket"
  ]
}

variable "cross_account_object_actions" {
  type        = list(string)
  description = "Actions on bucket objects to allow"
  default = [
    "s3:GetObject"
  ]
}

variable "cross_account_object_actions_with_forced_acl" {
  type        = list(string)
  description = "Actions on bucket objects to allow only with forced acl"
  default = [
    "s3:PutObject",
    "s3:PutObjectAcl"
  ]
}

variable "cross_account_forced_acls" {
  type        = list(string)
  description = "ACLs to force on new objects for cross account xs"
  default = [
    "bucket-owner-full-control"
  ]
}

variable "origin_access_identities" {
  type        = list(string)
  description = "Cloudfront Origin Access Identities to grant read-only access to."
  default     = []
}

variable "create_origin_access_identity" {
  type        = bool
  description = "Whether to create an origin access identity (OAI) and policy to be accessible from Cloudfront."
  default     = false
}

# --------------------------------------------------------------------
# Cross Origin Resource Sharing (CORS)
# --------------------------------------------------------------------
variable "create_cross_origin_resource_sharing" {
  type        = bool
  description = "Whether to create an origin access identity (OAI) and policy to be accessible from Cloudfront."
  default     = false
}

variable "cors_bucket" {
  type        = string
  description = "(Required, Forces new resource) The name of the bucket."
  default     = null
}

variable "cors_allowed_headers" {
  type        = list(string)
  description = "(Optional) Set of Headers that are specified in the Access-Control-Request-Headers header."
  default     = ["*"]
}

variable "cors_allowed_methods" {
  type        = list(string)
  description = "(Required) Set of HTTP methods that you allow the origin to execute. Valid values are GET, PUT, HEAD, POST, and DELETE."
  default     = ["PUT", "POST"]
}

variable "cors_allowed_origins" {
  type        = list(string)
  description = "(Required) Set of origins you want customers to be able to access the bucket from."
  default     = ["*"]
}

variable "cors_expose_headers" {
  type        = list(string)
  description = "(Optional) Set of headers in the response that you want customers to be able to access from their applications (for example, from a JavaScript XMLHttpRequest object)."
  default     = []
}

variable "cors_id" {
  type        = string
  description = "(Optional) Unique identifier for the rule. The value cannot be longer than 255 characters."
  default     = null
}

variable "cors_max_age_seconds" {
  type        = string
  description = "(Optional) The time in seconds that your browser is to cache the preflight response for the specified resource."
  default     = null
}

