variable "create_internet_gateway" {
  type = bool
  default = false
}

variable "vpc_id" {
  type = string
  default = ""
}

variable "tags" {
  type = map(string)
  default = "self_test"
}

variable "create_nat_gateway" {
  type = bool
  default = false
}
variable "create_vpc_endpoint" {
    type = bool
    default = false
}
variable "allocation_id"{
    type=string
    default=""
}
variable "connectivity_type" {
    type=string
    default=""
}
variable "private_ip" {
  type=string
  default=""
}
variable "service_name" {
    type = string
    default = ""
}
variable "auto_accept" {
    type = bool
  default = false
}
variable "policy" {
  type = string
  default = ""
}
variable "route_table_ids " {
  type = string
  default = ""
}
variable "ip_address_type" {
  type = string
  default= "ipv4"
}
variable "subnet_ids" {
  type = list(string)
  default = []
}
variable "security_group_ids" {
  type = string
  default = ""
}
variable "vpc_endpoint_type" {
    type = string
    default = ""

}