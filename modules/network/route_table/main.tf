resource "aws_route_table" "this" {
    count = var.create_route_table ? 1: 0
    vpc_id = var.vpc_id
  
  tags = var.tags
}

resource "aws_route" "this" {
    count = var.create_route ? 1: 0
    route_table_id = aws_route_table.this.*.id
    destination_cidr_block = var.destination_cidr_block 
    gateway_id = var.gateway_id
    nat_gateway_id = var.nat_gateway_id
    vpc_endpoint_id = var.vpc_endpoint_id
    vpc_peering_connection_id = var.vpc_peering_connection_id
  
}

resource "aws_route_table_association" "this" {
    count = var.create_route_associatiom ? 1: 0
    route_table_id = aws_route_table.this.*.id
    subnet_id = var.subnet_id
    gateway_id = var.gateway_id
  
}