output "vpc_arn" {
  value = try(aws_vpc.this.*.arn, "")
} 
output "vpc_id" {
  value = try(aws_vpc.this.*.id, "")
}

#outputs for subnet
output "public_subnet_id" {
  value = try(aws_subnet.public_subnet.*.id, "")
}
output "public_subnet_arn" {
    value = try(aws_subnet.public_subnet.*.arn, "")
}

output "private_subnet_id" {
  value = try(aws_subnet.private_subnet.*.id, "")
}
output "private_subnet_arn" {
    value = try(aws_subnet.private_subnet.*.arn, "")
}

output "database_subnet_id" {
  value = try(aws_subnet.database_subnet.*.id, "")
}
output "database_subnet_arn" {
    value = try(aws_subnet.database_subnet.*.arn, "")
}
