locals{
    name = "self-test"
}
resource "aws_vpc" "this" {
    count = var.create_vpc ? 1 : 0
    cidr_block = var.vpc_cidr_block
    enable_dns_support = var.enable_dns_support
    enable_dns_hostnames = var.enable_dns_hostnames
    tags = var.tags

}

#public_subnet
resource "aws_subnet" "public_subnet" {
 count = var.create_subnet && length(var.public_cidr_block) != 0 ? length(var.public_cidr_block) : 0

 availability_zone = var.availability_zone[count.index] 
 cidr_block = var.public_cidr_block[count.index]
 vpc_id = aws_vpc.this[0].id
 tags = {
    Name = "${local.name}-public-${count.index}-subnet"
 }
}

#private_subnet
resource "aws_subnet" "private_subnet" {
 count = var.create_subnet && length(var.private_cidr_block) != 0 ? length(var.private_cidr_block) : 0

 availability_zone = var.availability_zone[count.index] 
 cidr_block = var.private_cidr_block[count.index]
 vpc_id = aws_vpc.this[0].id
 tags = {
    Name = "${local.name}-private-${count.index}-subnet"
 }
}

#database_subnet
resource "aws_subnet" "database_subnet" {
 count = var.create_subnet && length(var.database_cidr_block) != 0 ? length(var.database_cidr_block) : 0

 availability_zone = var.availability_zone[count.index] 
 cidr_block = var.database_cidr_block[count.index]
 vpc_id = aws_vpc.this[0].id
 tags = {
    Name = "${local.name}-database-${count.index}-subnet"
 }
}
