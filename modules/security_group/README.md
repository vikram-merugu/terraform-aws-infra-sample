# Table of content security groups

## On this page

1. [General Description](#general-description)
2. [Entire Example Usage with Datatypes](#entire-example-usage-with-datatypes)
3. [Examples & Possible Values](#examples--possible-values)  
   3.1 [SG with ingress and egress rule selected from predefined set of rules](#sg-with-ingress-and-egress-rule-selected-from-predefined-set-of-rules)  
   3.2 [SG with multiple cidr blocks and multiple pre defined rules](#sg-with-multiple-cidr-blocks-and-multiple-pre-defined-rules)  
   3.3 [SG with predefined ingress rule and custom rules](#sg-with-predefined-ingress-rule-and-custom-rules)  
   3.4 [SG with source security group and predefined rules](#sg-with-source-security-group-and-predefined-rules)   
   3.5 [SG with source security group and custom rules](#sg-with-source-security-group-and-custom-rules)  
4. [Outputs](#outputs)
5. [Inputs](#inputs)
6. [Used Resources](#used-resources)
7. [References](#references)
8. [Authors](#authors)
9. [Contacts](#contacts)

---

## General Description

This module has reusable security group with different sets of rules such as rule with ipv4 cidr block and for another security group.

([back to top](#table-of-content-security-groups))

---

## Entire Example Usage with Datatypes

```

module "example" {
  source                = "../../../modules/general/security-group"
  create_security_group = bool
  security_group_name   = string

  vpc_id                = string
  tags                  = map(string)
  
  create_security_group_ingress_rule = bool
  ingress_rules = [{
    "rules" = string
    "cidr" = string
    "description" = string
  }]
  ingress_custom_rules = [
  {
      "cidr" = string
      "from_port"   = string
      "to_port"     = string
      "protocol"    = string
      "description" = string
    }]
    
  create_security_group_ingress_rule_security_group = bool
  ingress_rules_with_security_group = [{
    "rules" = string
    "source_security_group_id" = string
    "description" = string
  }]
  ingress_custom_rules_security_group = [
    {
      "source_security_group_id" = string
      "from_port"   = string
      "to_port"     = string
      "protocol"    = string
      "description" = string
    }]
}

```

([back to top](#table-of-content-security-groups))

---

## Examples & Possible Values

### Security group Example

#### SG with ingress and egress rule selected from predefined set of rules

```
module "test-sg" {
  source = "../../modules/general/sg"

  create_security_group = true
  security_group_name = lower("test-SG")
  vpc_id = data.terraform_remote_state.network.outputs.vpc_id
  tags = local.tags
  create_security_group_ingress_rule = true
  ingress_rules = [{
    "rules" = "http-80-tcp"
    "cidr" = "0.0.0.0/0"
    "description" = "test-sg-description"
  }]
  create_security_group_egress_rule = local.create_security_group_egress_rule
  egress_custom_rules = [
  {
  "rules" = "http-80-tcp"
    "cidr" = "0.0.0.0/0"
    "description" = "test-sg-description"
  }]
}

```

([back to top](#table-of-content-security-groups))

#### SG with multiple cidr blocks and multiple pre defined rules

```
module "test-sg" {
  source = "../../modules/general/sg"
  create_security_group = true
  security_group_name = lower("test-SG")
  vpc_id = data.terraform_remote_state.network.outputs.vpc_id
  tags = local.tags

  create_security_group_ingress_rule = true
  ingress_rules = [{
    "rules" = "http-80-tcp"
    "cidr" = "0.0.0.0/0"
    "description" = "test-sg-description"
  },
  {
    "rules" = "http-8080-tcp"
    "cidr" = "0.0.0.0/0"
    "description" = "test-sg-description"
    }]
  create_security_group_egress_rule = true
  egress_custom_rules = [
  {
   "rules" = "http-80-tcp"
   "cidr" = "0.0.0.0/0"
   "description" = "test-sg-description"
  },
  {
  "rules" = "http-8080-tcp"
   "cidr" = "0.0.0.0/0"
   "description" = "test-sg-description"
  }]
}
```

([back to top](#table-of-content-security-groups))

#### SG with predefined ingress rule and custom rules

```
module "test-sg" {
  source = "../../modules/general/sg"

  create_security_group = true
  security_group_name = lower("test-SG")
  vpc_id = data.terraform_remote_state.network.outputs.vpc_id
  tags = local.tags

  create_security_group_ingress_rule = true
  ingress_rules = [{
    "rules" = "http-80-tcp"
    "cidr" = "0.0.0.0/0"
    "description" = "test-sg-description"
  },
   {
     "rules" = "http-8080-tcp"
    "cidr" = "0.0.0.0/0"
    "description" = "test-sg-description"
    }]
  ingress_custom_rules = [
  {
      "cidr" = "0.0.0.0/0"
      "from_port"   = "23"
      "to_port"     = "23"
      "protocol"    = "tcp"
      "description" = "test_sg"
    }]
}
```

([back to top](#table-of-content-security-groups))

#### SG with source security group and predefined rules

```
module "example" {
  source = "../../modules/general/sg"
  create_security_group = true
  security_group_name = lower("sample")
  vpc_id = data.terraform_remote_state.network.outputs.vpc_id
  tags = local.tags

  create_security_group_ingress_rule_security_group = true
  ingress_rules_with_security_group = [{
    "rules" = "http-80-tcp"
    "source_security_group_id" = module.https-sg.security_group_id
    "description" = "test-sg-description"
  }]
}

```

([back to top](#table-of-content-security-groups))

#### SG with source security group and custom rules  

```
module "example" {
  source = "../../modules/general/sg"
  create_security_group = true
  security_group_name = lower("sample")
  vpc_id = data.terraform_remote_state.network.outputs.vpc_id
  tags = local.tags

  create_security_group_ingress_rule_security_group = true
  ingress_custom_rules_security_group = [
    {
      "source_security_group_id" = module.https-sg.security_group_id
      "from_port"   = "23"
      "to_port"     = "23"
      "protocol"    = "tcp"
      "description" = "test_sg with another sg"
    }]
    }
    
```

---

## Outputs

| Name                    | Description                           | Type   |
|-------------------------|---------------------------------------|--------|
| security_group_id       | Id that identifies the security group | string |
| security_group_arn      | ARN of security group                 | string |
| security_group_vpc_id   | The VPC ID                            | string |
| security_group_owner_id | The owner ID                          | string |
| security_group_name     | The name of the security group        | string |

([back to top](#table-of-content-security-groups))

---

## Inputs

| Name                                              | Description                                                                                                                                         | type           | default |
|---------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------|----------------|---------|
| create_security_group                             | Determines whether resources will be created                                                                                                        | bool           | false   |
| tags                                              | A map of tags to add to all resources                                                                                                               | string         | null    |
| security_group_name                               | Name of the security group                                                                                                                          | string         | null    |
| vpc_id                                            | The VPC on which security group has to be created                                                                                                   | string         | null    |
| create_security_group_rule_ingress                | Determines whether ingress security group rule will be created                                                                                      | bool           | false   |
| ingress_rules                                     | List of ingress rules to create by name, requires 'rules' and 'cidr'                                                                                | list(map(any)) | []      |
| create_security_group_rule_egress                 | Determines whether to create egresssecurity group rule                                                                                              | bool           | false   |                 
| egress_rules                                      | List of egress rules to create by name, requires 'rules' and 'cidr'                                                                                 | list(map(any)) | []      |
| create_security_group_ingress_rule_security_group | Determines whether to create security group rule with another security group                                                                        | bool           | false   |
| ingress_rules_with__security_group                | List of rules to create where 'source_security_group_id' is used, requires 'rules', 'description' and 'source_security_group_id'                    | list(map(any)) | []      |
| ingress_custom_rules                              | to define ingress ports which is not in rules list, should contains 'from_port','to_port', 'protocol', 'description', 'cidr'                        | list(map(any)) | []      | 
 | egress_custom_rules                               | to define egress ports which is not in rules list, should contains 'from_port', 'to_port', 'protocol','description' and 'cidr'                      | list(map(any)) | []      |
 | ingress_custom_rules_security_group               | to define ingress ports which is not in rules list, should contains 'from_port', 'to_port', 'protocol','description' and 'source_security_group_id' | list(map(any)) | []      |
 | egress_custom_rules_security_group                | to define egress ports which is not in rules list, should contains 'from_port', 'to_port', 'protocol','description' and 'source_security_group_id'  | list(map(any)) | []      |                                                                           |

([back to top](#table-of-content-security-groups))

---

## Used Resources

### Security group

- aws_security_group
- aws_security_group_rule

([back to top](#table-of-content-security-groups))

---

## References

- [Terraform security group](https://github.com/terraform-aws-modules/terraform-aws-security-group/blob/master/main.tf)
- [AWS Security group](https://docs.aws.amazon.com/managedservices/latest/userguide/about-security-groups.html)

([back to top](#table-of-content-security-groups))

---

## Authors

- This module was originally created by **Anu Oommen** 

([back to top](#table-of-content-security-groups))

---

## Contacts

- Anu - <anusoommen@gmail.com>


([back to top](#table-of-content-security-groups))