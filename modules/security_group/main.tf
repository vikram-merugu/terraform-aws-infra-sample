#==============================================================
# Creation of Security group
#==============================================================

resource "aws_security_group" "this" {
  /* AWS security group creation
     :param create_security_group
     :param security_group_name
     :param vpc_id
  */
  count = var.create_security_group ? 1 : 0

  name   = var.security_group_name
  vpc_id = var.vpc_id


  lifecycle {
    create_before_destroy = false
  }
  tags = merge(
    {
      "Name" = format("%s", var.security_group_name)
    },
    var.tags,
  )
}

#===========================================================
# creation of security group rule -  ingress with predefined rules
#===========================================================
resource "aws_security_group_rule" "ingress_rules_with_pre_defined_rules" {
  /* Security rule for ipv4 cidr block in ingress
     :param create_security_group_ingress_rule
     :param ingress_rules
     :param ingress_cidr_blocks

  */
  count = var.create_security_group && var.create_security_group_ingress_rule ? length(var.ingress_rules) : 0

  security_group_id = aws_security_group.this[0].id
  type              = "ingress"

  description = try(lookup(var.ingress_rules[count.index], "description"),var.rules[lookup(var.ingress_rules[count.index],"rules")][3])
  cidr_blocks = [lookup(var.ingress_rules[count.index],"cidr")]

  from_port = var.rules[lookup(var.ingress_rules[count.index],"rules")][0]
  to_port   = var.rules[lookup(var.ingress_rules[count.index],"rules")][1]
  protocol  = var.rules[lookup(var.ingress_rules[count.index],"rules")][2]

}

#----------------------------------------------------------------------------------------------------------------
#creation of security group rule - egress with predefined rules
#----------------------------------------------------------------------------------------------------------------

resource "aws_security_group_rule" "egress_rules_cidr_block" {
  /* Security rule for ipv4 cidr block in ingress
     :param create_security_group_egress_rule
     :param egress_rules
     :param egress_cidr_blocks

  */
  count = var.create_security_group && var.create_security_group_egress_rule ? length(var.egress_rules) : 0

  security_group_id = aws_security_group.this[0].id
  type              = "egress"

  description = try(lookup(var.egress_rules[count.index], "description"),var.rules[lookup(var.egress_rules[count.index],"rules")][3])
  cidr_blocks = [lookup(var.egress_rules[count.index],"cidr")]

  from_port = var.rules[lookup(var.egress_rules[count.index],"rules")][0]
  to_port   = var.rules[lookup(var.egress_rules[count.index],"rules")][1]
  protocol  = var.rules[lookup(var.egress_rules[count.index],"rules")][2]

}

#-----------------------------------------------------------------------
# security group rule - custom ingress ports
#-----------------------------------------------------------------------
resource "aws_security_group_rule" "ingress_with_custom_rules" {
  /* Security rule for ipv4 cidr block in ingress
     :param create_security_group_ingress_rule
     :param ingress_custom_rules
     :param ingress_cidr_blocks

  */
  count = var.create_security_group && var.create_security_group_ingress_rule && (length(var.ingress_custom_rules) != 0) ? length(var.ingress_custom_rules) : 0

  security_group_id = aws_security_group.this[0].id
  type              = "ingress"

  description = lookup(var.ingress_custom_rules[count.index], "description")
  cidr_blocks = [lookup(var.ingress_custom_rules[count.index], "cidr")]

  from_port = lookup(var.ingress_custom_rules[count.index], "from_port")
  to_port   = lookup(var.ingress_custom_rules[count.index], "to_port")
  protocol  = lookup(var.ingress_custom_rules[count.index], "protocol")

}

#-----------------------------------------------------------------------
# security group rule - custom egress ports
#-----------------------------------------------------------------------
resource "aws_security_group_rule" "egress_with_custom_rules" {
  /* Security rule for ipv4 cidr block in egress
     :param create_security_group_egress_rule
     :param egress_rules
     :param egress_cidr_blocks

  */
  count = var.create_security_group && var.create_security_group_egress_rule && (length(var.egress_custom_rules) != 0) ? length(var.egress_custom_rules) : 0

  security_group_id = aws_security_group.this[0].id
  type              = "egress"

  description = lookup(var.egress_custom_rules[count.index], "description")
  cidr_blocks = [lookup(var.egress_custom_rules[count.index], "cidr")]

  from_port = lookup(var.egress_custom_rules[count.index], "from_port")
  to_port   = lookup(var.egress_custom_rules[count.index], "to_port")
  protocol  = lookup(var.egress_custom_rules[count.index], "protocol")

}

#=======================================================================================
# creation of security group rule - ingress another security group with custom ports
#=======================================================================================

resource "aws_security_group_rule" "custom_rules_security_group" {
  /* Security rule for security group in ingress
     :param create_security_group_ingress_rule_security_group
     :param ingress_custom_rules
     :param ingress_source_security_group_id
  */

  count = var.create_security_group && var.create_security_group_ingress_rule_security_group && (length(var.ingress_custom_rules_security_group) != 0) ? length(var.ingress_custom_rules_security_group) : 0

  security_group_id = aws_security_group.this[0].id
  type              = "ingress"

  source_security_group_id = lookup(var.ingress_custom_rules_security_group[count.index], "source_security_group_id")
  description = lookup(var.ingress_custom_rules_security_group[count.index], "description")

  from_port = lookup(var.ingress_custom_rules_security_group[count.index], "from_port")
  to_port   = lookup(var.ingress_custom_rules_security_group[count.index], "to_port")
  protocol  = lookup(var.ingress_custom_rules_security_group[count.index], "protocol")
}

#=======================================================================================
# creation of security group rule - ingress another security group with pre defined ports
#=======================================================================================

resource "aws_security_group_rule" "predefined_rules_security_group" {
  /* Security rule for security group in ingress
     :param create_security_group_ingress_rule_security_group
     :param ingress_rules
     :param ingress_source_security_group_id

  */
  count = var.create_security_group && var.create_security_group_ingress_rule_security_group ? length(var.ingress_rules_with_security_group) : 0

  security_group_id = aws_security_group.this[0].id
  type              = "ingress"

  description = try(lookup(var.ingress_rules_with_security_group[count.index], "description"),var.rules[lookup(var.ingress_rules_with_security_group[count.index], "rules")][3])
  source_security_group_id = lookup(var.ingress_rules_with_security_group[count.index], "source_security_group_id")

  from_port = var.rules[lookup(var.ingress_rules_with_security_group[count.index], "rules")][0]
  to_port   = var.rules[lookup(var.ingress_rules_with_security_group[count.index], "rules")][1]
  protocol  = var.rules[lookup(var.ingress_rules_with_security_group[count.index], "rules")][2]
}
