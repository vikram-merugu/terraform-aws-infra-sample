variable "create_dynamodb_table" {
  type = bool
  description = "whether to create dynamodb"
  default = false
}
variable "attribute" {
  type = list(map(string))
  default = []
}
variable "hash_key" {
  type = string
  default = ""
}
variable "name" {
  type = string
  default = ""
}
variable "tags" {
  type = map(string)
  default = {
  Name = "self_test"}
}
