output "dynamodb_id" {
  value = try(aws_dynamodb_table.this.*.id, "")
}

output "dynamodb_arn" {
  value = try(aws_dynamodb_table.this.*.arn, "")
}
