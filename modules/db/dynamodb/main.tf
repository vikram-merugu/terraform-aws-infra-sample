resource "aws_dynamodb_table" "this" {
  count = var.create_dynamodb_table ? 1 : 0
  dynamic "attribute" {
    for_each = var.attribute
    content {
      name = attribute.value.name
      type = attribute.value.type

    }
  } 
  hash_key = var.hash_key
  name = var.name
  billing_mode = "PAY_PER_REQUEST"
} 
