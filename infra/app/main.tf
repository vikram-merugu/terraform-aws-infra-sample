locals {
    name = "self-test"
    services = toset(["com.amazonaws.${var.region}.ecr.dkr", "com.amazonaws.${var.region}.ecr.api"])
}

module "network" {
    source = "../../modules/network/vpc"
    create_vpc = true
    vpc_cidr_block = "10.0.0.0/16"
    create_subnet = true
    availability_zone = ["${var.region}a", "${var.region}b"]
    public_cidr_block = ["10.0.1.0/24", "10.0.2.0/24"]
    private_cidr_block = ["10.0.3.0/24", "10.0.4.0/24"]
}

module "security_group" {
    source = "../../modules/security_group"
create_security_group = true 
    security_group_name = "${local.name}-lb-sg"
    vpc_id = module.network.vpc_id
    depends_on = [module.network]

}

module "loadbalancer" {
    source = "../../modules/loadbalancer"
    create_load_balancer = true
    name = "${local.name}-lb"
    internal = true
    security_group_ids = module.security_group.security_group_id
    depends_on = [
      module.security_group,
      module.network
    ]
    load_balancer_type = "application"
    subnet_ids = [module.network.private_subnet_id]
    http_listener = [
        {   
            port = 80
            protocol = "http"
            type = "fixed-response"
            fixed_response = {
                content_type = "text/plain"
                message_body = "nothing to display"
                status_code = 403
            }
        }
    ]
    http_listener_rules = [
        {
            priority = 1
            actions = [{
                type = "fixed-response"
                content_type = "text/plain"
                message_body = "nothing to display"
                status_code = 403
            }]
            conditions = [{
                http_header = [{
                    http_header_name = "X-Custom-Header"
                    values = ["anu"]
                }]
            }]
        }
    ]
    target_groups = [{
        name = "blue-${local.name}-tg"
        port = "80"
        protocol = "http"
        target_type = "ip"
        vpc_id = module.network.vpc_id
        health_check = {
            enabled = true
            healthy_threshold = 5
            interval = 25
            matcher = "200-299"
            path = "/"
            port = 80
            protocol = "http"
            unhealthy_threshold = 3
        }
    },
        {
        name = "green-${local.name}-tg"
        port = "8080"
        protocol = "http"
        target_type = "ip"
        vpc_id = module.network.vpc_id
        health_check = {
            enabled = true
            healthy_threshold = 5
            interval = 25
            matcher = "200-299"
            path = "/"
            port = 80
            protocol = "http"
            unhealthy_threshold = 3
        }
    }
    ]

    # create_target_group = true
    # name_tg = "self-test-tg"
   
}

module "ecs_cluster" {
    source = "../../modules/ecs"
    cluster_name = "${local.name}-cluster"
    create_ecs_cluster = true
    configuration = {
        execute_command_configuration = {
            logging = "OVERRIDE"
            log_configuration = {
                cloud_watch_encryption_enabled = true
                cloud_watch_log_group_name = "test"
            }
    }
    }
    capacity_providers = ["FARGATE", "FARGATE_SPOT"]
    default_capacity_provider_strategy = [{
        capacity_provider = "FARGATE"
        base = 1
      },
    {
    capacity_provider = "FARGATE_SPOT"
    weight = 2
    }]
    create_ecs_service = true
    service_name = "${local.name}-service"
    deployment_circuit_breaker = {
        enable = true
        rollback = true
    }
    deployment_controller = {
        type = "ECS"
    }
    deployment_maximum_percent = 100
    deployment_minimum_healthy_percent = 50
    desired_count = 1
    enable_execute_command = true
    health_check_grace_period_seconds = 120
    load_balancer = {
        elb_name = module.loadbalancer.load_balancer_arn
        target_group_arn = module.loadbalancer.target_group_arn
        container_name = "nginx"
        container_port =  3000
    }
    network_configuration = {
        subnets = module.network.private_subnet_id
        assign_security_groups = [module.security_group.security_group_id]
        assign_public_ip = false
    }

    #task_definition
    container_definitions = file("container_def.json")
    family = "nginx"
    cpu = 2
    execution_role_arn = ""
    memory = 256
    network_mode = "awsvpc"
    requires_compatibilities = ["FARGATE"]
    task_role_arn = ""

    #autoscaling_cpu
    create_autoscaling_fargate = true
    appautoscale_policy = [
        {
    autoscale_metrics_name = "CPU"
    max_scale_capacity = 2
    min_scale_capacity = 1
    policy_type = "TargetTrackingScaling"
    target_tracking_scaling_policy_configuration = [
        {
            target_value = 50
            scale_in_cooldown = 300
            scale_out_cooldown = 300
            predefined_metric_specification = [{
                predefined_metric_type = "ECSServiceAverageCPUUtilization"
            }]
        }]
        },
        {
    autoscale_metrics_name = "Memory"
    max_scale_capacity = 2
    min_scale_capacity = 1
    policy_type = "TargetTrackingScaling" 
    target_tracking_scaling_policy_configuration = [
        {
            target_value = 60
            scale_in_cooldown = 300
            scale_out_cooldown = 300
            predefined_metric_specification = [{
                predefined_metric_type = "ECSServiceAverageMemoryUtilization"
            }]
        }
    ]}
    ]
    depends_on = [module.ecr_repos]

}

#ECR repository
module "ecr_repos" {
    source = "../../modules/ecr"
    create_ecr_repos = true
    ecr_repository_name = local.name
    image_tag_mutability = "MUTABLE"
    image_scanning_configuration = {
        scan_on_push = true
    }
}

module "ecr_vpc_interface_endpoint" {
    source = "../../modules/private_link"
    create_vpc_endpoint = true
    for_each = local.services
    service_name = each.value
    vpc_id = module.network.vpc_id
    subnet_ids = [module.network.private_subnet_id]
    security_group_ids = [module.security_group.security_group_id]
    vpc_endpoint_type = "Interface"

}

module "codecommit_repos" {
    source = "../../modules/codecommit"
    create_codecommit_repository = true
    codecommit_repository_name = "${local.name}-repos"
}