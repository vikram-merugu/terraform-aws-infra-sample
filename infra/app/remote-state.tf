  terraform {
   backend "s3" {
    bucket = "terraform-state-s3-default"
    key = "app/terraform.tfstate"
    region = "us-west-2"
    dynamodb_table = "dynamodb-state-locking"
   }
 }
