locals {
    s3_bucket_name = "terraform-state-s3"
}
module "remote_state_s3" {
    source = "../../modules/s3"
    create_bucket = true
    name = local.s3_bucket_name
    versioning = {
      "status" = "Enabled"
    }
   suffix_name = terraform.workspace
   lifecycle_rules = [
    {
        id = "state-files"
        status = "Enabled"

        noncurrent_version_transition = [
            {
            noncurrent_days = 7
            storage_class = "GLACIER"
        }]
    }
   ]
   attach_policy_custom = true
   policy_custom = data.aws_iam_policy_document.this.json
}

data "aws_iam_policy_document" "this" {
 version = "2008-10-17"
 statement {
    actions = ["s3:ListBucket"]
    effect = "Allow"
    principals {
      identifiers = ["arn:aws:iam::xxxxx:role/assumerole-terraform-gitlab", "arn:aws:iam::075306354397:user/self-test-user"]
      type = "AWS"
    }
    resources = ["arn:aws:s3:::${local.s3_bucket_name}-${terraform.workspace}"]
 }
 statement {
    principals {
      identifiers = ["arn:aws:iam::xxxxxx:role/assumerole-terraform-gitlab", "arn:aws:iam::075306354397:user/self-test-user"]
      type = "AWS"
    }
    actions = ["s3:GetObject", "s3:PutObject", "s3:DeleteObject"]
    effect = "Allow"
    resources = ["arn:aws:s3:::${local.s3_bucket_name}-${terraform.workspace}/*"]
 }
}

module "dynamodb_state_locking" {
    source = "../../modules/db/dynamodb"
    create_dynamodb_table = true
    name = "dynamodb-state-locking"
    hash_key = "LockID"
    attribute = [{
        name = "LockID"
        type = "S"
        }]
}

