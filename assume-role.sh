#!/bin/bash
echo "assumerole details"
STC=$(aws sts assume-role --role-arn "arn:aws:iam::XXXXXXX:role/assumerole-terraform-gitlab" --role-session-name gitlab-runner --query '[Credentials.AccessKeyId,Credentials.SecretAccessKey,Credentials.SessionToken]' --output text)
set STC
AWS_ACCESS_KEY_ID=$1
AWS_SECRET_ACCESS_KEY=$2