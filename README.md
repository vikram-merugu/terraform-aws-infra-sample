# General Description

This is the one of the repository which I have been working in free time based on my experience and to help others with terraform, CI/CD pipeline and some of AWS resources.This is an initial version of IaC. It is still in progress and there is a chance of errors.

This project has reusable modules and I intended to create a sample application on ECS utilising these reusable component.

Pipeline mightn't work as the gitlab runner is in my private AWS account and it won't be running all the time.

([back to top](#general-description))

---

## References

- [Terraform Modules](https://github.com/terraform-aws-modules)

([back to top](#general-description))

---

## Authors

- This module was originally created by **Anu Oommen** and you can reach me anusoommen@gmail.com

([back to top](#general-description))

---

## Contacts

- Anu Oommen- <anusoommen@gmail.com>


([back to top](#general-description))   